function subtraction(num1,num2) {
  return num1 - num2;
}

function divide(num1, num2) {
  if (num2 !== 0) {
    return num1 / num2;
  } else {
    return "Error: Division by zero!";
  }
}

function multiplication(num1, num2) {
  return num1 * num2;
}

function add(num1, num2) {
  if (num2 !== 0) {
      return num1 + num2;
  } else {
      return 'Error: Addition with zero!';
  }
}

console.log(add(10, 2));
console.log(divide(10, 2));
console.log(multiplication(10, 2));
console.log(subtraction(10,2));